#include "ttt.hpp"
#include <ftxui/screen/color.hpp>

ttt::ttt() : moves(0), finished(state::empty) {
    for (int i = 0; i < 9; i++) {
        arr[i] = state::empty;
        labels[i] = " ";
        fgs[i] = ftxui::Color::Default;
        bgs[i] = ftxui::Color::Default;
        his[i] = ftxui::Color::Blue;
    }
    p1_fg = ftxui::Color::Red;
    p2_fg = ftxui::Color::Green;
}

void ttt::handle_win() {
    if (arr[0] == state::pl_1 && arr[1] == state::pl_1 &&
        arr[3] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[3] == state::pl_1 && arr[4] == state::pl_1 &&
               arr[5] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[6] == state::pl_1 && arr[7] == state::pl_1 &&
               arr[8] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[0] == state::pl_1 && arr[3] == state::pl_1 &&
               arr[6] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[1] == state::pl_1 && arr[4] == state::pl_1 &&
               arr[7] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[2] == state::pl_1 && arr[5] == state::pl_1 &&
               arr[8] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[0] == state::pl_1 && arr[4] == state::pl_1 &&
               arr[8] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[2] == state::pl_1 && arr[4] == state::pl_1 &&
               arr[6] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[2] == state::pl_1 && arr[5] == state::pl_1 &&
               arr[8] == state::pl_1) {
        finished = state::pl_1;
    } else if (arr[0] == state::pl_2 && arr[1] == state::pl_2 &&
               arr[3] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[3] == state::pl_2 && arr[4] == state::pl_2 &&
               arr[5] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[6] == state::pl_2 && arr[7] == state::pl_2 &&
               arr[8] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[0] == state::pl_2 && arr[3] == state::pl_2 &&
               arr[6] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[1] == state::pl_2 && arr[4] == state::pl_2 &&
               arr[7] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[2] == state::pl_2 && arr[5] == state::pl_2 &&
               arr[8] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[0] == state::pl_2 && arr[4] == state::pl_2 &&
               arr[8] == state::pl_2) {
        finished = state::pl_2;
    } else if (arr[2] == state::pl_2 && arr[4] == state::pl_2 &&
               arr[6] == state::pl_2) {
        finished = state::pl_2;
    }
}
