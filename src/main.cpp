#include "disp.hpp"
#include <ftxui/dom/elements.hpp>
#include <ftxui/screen/screen.hpp>

int main() {
    using namespace ftxui;
    Disp D;
    D.Draw();
    return EXIT_SUCCESS;
}
