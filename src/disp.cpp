#include "disp.hpp"
#include <cstdint>
#include <ftxui-grid-container/grid-container.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <ftxui/dom/deprecated.hpp>
#include <ftxui/dom/elements.hpp>
#include <ftxui/dom/node.hpp>
#include <string>
#include <utility>
#include <vector>

ftxui::ComponentDecorator
Disp::CatchEvents(std::pair<std::uint8_t, std::uint8_t> cursor) {
    using namespace ftxui;
    return CatchEvent([=, this](Event event) {
        if (event == Event::Character('\n')) {
            if (table.finished != ttt::state::empty) {
                return true;
            }
            if (table.arr[cursor.first * 3 + cursor.second] ==
                ttt::state::empty) {
                table.moves++;
                table.arr[cursor.first * 3 + cursor.second] =
                    (table.moves & 1) ? ttt::state::pl_1 : ttt::state::pl_2;
                table.labels[cursor.first * 3 + cursor.second] =
                    (table.moves & 1) ? "X" : "O";
                table.fgs[cursor.first * 3 + cursor.second] =
                    (table.moves & 1) ? table.p1_fg : table.p2_fg;
                table.handle_win();
                switch (table.finished) {
                case ttt::state::empty: answer = "Game in progress"; break;
                case ttt::state::pl_1: answer = "Player 1 has won!"; break;
                case ttt::state::pl_2: answer = "Player 2 has won!"; break;
                }
                return true;
            }
        }
        return false;
    });
}
ftxui::Component Disp::Cell(std::string &label, const ftxui::Color &fg,
                            const ftxui::Color &hi) {
    using namespace ftxui;
    auto cell = Renderer([&](bool focused) {
        auto style = [](Element Elem) {
            return Elem | center | flex | size(WIDTH, GREATER_THAN, 25) |
                   size(HEIGHT, GREATER_THAN, 25);
        };
        if (focused) {
            return text(label) | bold | color(hi) | style |
                   borderStyled(BorderStyle::ROUNDED, hi);
            ;
        } else {
            return text(label) | color(fg) | style |
                   borderStyled(BorderStyle::ROUNDED, fg);
        }
    });
    return cell;
}
Disp::Disp() : table(), screen(ftxui::ScreenInteractive::Fullscreen()) {
    using namespace ftxui;
    std::vector<Components> buttons(3);
    for (std::uint8_t i = 0; i < 3; i++) {
        buttons[i].push_back(
            Cell(table.labels[3 * i], table.fgs[3 * i], table.his[3 * i]) |
            CatchEvents({i, 0}) | center | flex);
        buttons[i].push_back(Cell(table.labels[3 * i + 1], table.fgs[3 * i + 1],
                                  table.his[3 * i + 1]) |
                             CatchEvents({i, 1}) | center | flex);
        buttons[i].push_back(Cell(table.labels[3 * i + 2], table.fgs[3 * i + 2],
                                  table.his[3 * i + 2]) |
                             CatchEvents({i, 2}) | center | flex);
    }
    auto style = [](Element Elem) {
        return Elem | center | flex | size(WIDTH, GREATER_THAN, 25) |
               borderRounded;
    };
    switch (table.finished) {
    case ttt::state::empty: answer = "Game in progress"; break;
    case ttt::state::pl_1: answer = "Player 1 has won!"; break;
    case ttt::state::pl_2: answer = "Player 2 has won!"; break;
    }
    grid = Container::Horizontal(
        {GridContainer(buttons) | center | flex, Renderer([&] {
             return vbox({
                        text("Player 1: " + table.player1) | style |
                            color(table.p1_fg),
                        text("Player 2: " + table.player2) | style |
                            color(table.p2_fg),
                        text(answer) | style | color(Color::GrayDark),
                    }) |
                    style;
         })});

    grid |= CatchEvent([=, this](Event event) {
        if (event == Event::Character('q')) {
            screen.ExitLoopClosure()();
        }
        return false;
    });
    InputOption options = InputOption::Spacious();
    options.transform = [](InputState state) {
        state.element |= color(Color::GrayDark);
        state.element |= bgcolor(Color::Default);
        if (state.focused) {
            state.element |= bold;
        }
        return state.element;
    };

    auto inp1 = Input(&table.player1, "Player 1", options) | style;
    inp1 |= CatchEvent([&](Event event) {
        return event == Event::Character('\n') ||
               event == Event::Character(' ');
    });
    auto inp2 = Input(&table.player2, "Player 2", options) | style;
    inp2 |= CatchEvent([&](Event event) {
        return event == Event::Character('\n') ||
               event == Event::Character(' ');
    });
    auto Okay = Button(
                    "Continue",
                    [&]() {
                        if (!table.player1.empty() && !table.player2.empty()) {
                            screen.ExitLoopClosure()();
                        }
                    },
                    ButtonOption::Animated(Color::Default, Color::Default,
                                           Color::Default, Color::GrayDark)) |
                style;
    nametaker = Container::Vertical({inp1, inp2, Okay}) | center | flex;
}

void Disp::Draw() {
    screen.Loop(nametaker);
    if (table.player1.empty() || table.player2.empty()) {
        return;
    }
    screen.Loop(grid);
}
