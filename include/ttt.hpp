#include <ftxui/screen/color.hpp>
#include <string>
struct ttt {
    enum class state : char { empty, pl_1, pl_2 };
    state arr[9];
    std::string player1, player2;
    std::string labels[9];
    ftxui::Color fgs[9];
    ftxui::Color bgs[9];
    ftxui::Color his[9];
    ftxui::Color p1_fg;
    ftxui::Color p2_fg;
    std::uint8_t moves;
    state finished;
    ttt();
    void handle_win();
};
