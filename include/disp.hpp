#include "ttt.hpp"
#include <ftxui-grid-container/grid-container.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <string>
#include <utility>
class Disp {
  public:
    Disp();
    void Draw();
    // ~Disp();

  private:
    ftxui::ComponentDecorator
    CatchEvents(std::pair<std::uint8_t, std::uint8_t> cursor);
    ftxui::Component Cell(std::string &label, const ftxui::Color &fg,
                          const ftxui::Color &hi);
    ftxui::Component grid;
    ftxui::Component nametaker;
    ttt table;
    ftxui::ScreenInteractive screen;
    std::string answer;
};
